using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System.Web.Http;
using Billboard.Data.MongoDb.Contract;
using Billboard.Data.MongoDb;
using Billboard.Cache.WebApiForMongoDb.Infrastructure.Contract;
using Billboard.Cache.WebApiForMongoDb.Infrastructure.Realization;

namespace Billboard.Cache.WebApiForMongoDb.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }

        private static void RegisterTypes(UnityContainer container)
        {
            container.RegisterType<IActorRepository, ActorRepository>();
            container.RegisterType<IMyActorRepository, MyActorRepository>();
        }
    }
}
