﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Cache.WebApiForMongoDb.Models
{
    public class Actor
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string PhotoImg { get; set; }
        public IList<Movie> Movies { get; set; }
        public IList<Serial> Serials { get; set; }
    }
}
