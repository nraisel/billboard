﻿using Billboard.Cache.WebApiForMongoDb.Infrastructure.Contract;
using Billboard.Cache.WebApiForMongoDb.Models;
using Billboard.Data.MongoDb.Contract;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Billboard.Cache.WebApiForMongoDb.Controllers
{
    public class ActorController : ApiController
    {
        private IMyActorRepository _myActorRepository;
        public ActorController(IMyActorRepository myActorRepository)
        {
            _myActorRepository = myActorRepository;
        }

        // GET: Actor
        public IHttpActionResult Get()
        {
            try
            {
                var result = _myActorRepository.Get().ToList();
                if (result == null)
                    return BadRequest();
                else
                    return Ok(result);
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult AddActor(Actor actor)
        {
            try
            {
                var result = _myActorRepository.Add(actor);
                if (result == null)
                    return BadRequest();
                else
                    return Ok(result);
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }
    }
}