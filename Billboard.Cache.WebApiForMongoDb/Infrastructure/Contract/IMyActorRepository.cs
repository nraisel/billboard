﻿using Billboard.Cache.WebApiForMongoDb.Models;
using Billboard.Core.Common.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Cache.WebApiForMongoDb.Infrastructure.Contract
{
    public interface IMyActorRepository : IRepository<Actor>
    {
    }
}
