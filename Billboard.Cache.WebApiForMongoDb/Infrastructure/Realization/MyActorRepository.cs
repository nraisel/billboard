﻿using Billboard.Cache.WebApiForMongoDb.Infrastructure.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billboard.Cache.WebApiForMongoDb.Models;
using Billboard.Data.MongoDb.Contract;

namespace Billboard.Cache.WebApiForMongoDb.Infrastructure.Realization
{
    public class MyActorRepository : IMyActorRepository
    {
        private IActorRepository _mongoDbActorRepository;
        public MyActorRepository(IActorRepository mongoDbActorRepository)
        {
            _mongoDbActorRepository = mongoDbActorRepository;
        }
        public Actor Add(Actor entity)
        {
            var actor = new Billboard.Business.Entities.MongoDb.Model.Actor
            {
                Name = entity.Name,
                PhotoImg = entity.PhotoImg,
                Movies = null,
                Serials = null
            };
            var result = _mongoDbActorRepository.Add(actor);
            return new Actor
            {
                Id = result.Id,
                Name = result.Name,
                PhotoImg = result.PhotoImg,
                Movies = null,
                Serials = null
            };
        }

        public IQueryable<Actor> Get()
        {
            var actors = new List<Actor>();
            var result = _mongoDbActorRepository.GetEnumerator();
            while (result.MoveNext())
            {
                var item = result.Current;
                var actor = new Actor
                {
                    Id = item.Id,
                    Name = item.Name,
                    PhotoImg = item.PhotoImg,
                    Movies = null,
                    Serials = null
                };
                actors.Add(actor);
            }
            return actors.AsQueryable();
        }

        public Actor GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public void Remove(Actor entity)
        {
            throw new NotImplementedException();
        }

        public Actor Update(Actor entity)
        {
            throw new NotImplementedException();
        }
    }
}
