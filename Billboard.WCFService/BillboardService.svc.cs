﻿using Billboard.Data.Billboard.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Billboard.WCFService.Model;
using Billboard.Core.Common;
using Billboard.WCFService.DTO;
using Billboard.Data.Billboard.Data.Realization;

namespace Billboard.WCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class BillboardService : IBillboardService
    {
        private IActorRepository _actorRepository;
        public BillboardService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }

        public BillboardService()
        {
            if (_actorRepository == null)
                _actorRepository = new ActorRepository();
        }

        public string GetActors()
        {
            try
            {
                var actorList = new List<Actor>();
                var actorListObj = new ActorList();
                var actors = _actorRepository.Get();
                var result = string.Empty;
                foreach (var actor in actors)
                {
                    var a = new Actor { Id = actor.Id, Name = actor.Name, PhotoImg = actor.PhotoImg };
                    actorList.Add(a);
                }
                actorListObj.Actors = actorList;
                return SerializerHelper.XmlSerializer<ActorList>(actorListObj);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
