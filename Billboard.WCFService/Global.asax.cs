﻿using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Ninject;
using Billboard.Data.Billboard.Data.Contract;
using Billboard.Data.Billboard.Data.Realization;

namespace Billboard.WCFService
{
    public class Global : NinjectHttpApplication
    {
        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<IActorRepository>().To<ActorRepository>().InSingletonScope();
            return kernel;
        }
    }
}