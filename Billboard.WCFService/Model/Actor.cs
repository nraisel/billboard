﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Billboard.WCFService.Model
{
    [XmlRoot("Actor")]
    [Serializable()]
    //[DataContract]
    public class Actor
    {
        //[DataMember]
        [XmlElement]
        public virtual int Id { get; set; }
        //[DataMember]
        [XmlElement]
        public virtual string Name { get; set; }
        //[DataMember]
        [XmlElement]
        public virtual string PhotoImg { get; set; }

        public Actor()
        {
                
        }
    }
}
