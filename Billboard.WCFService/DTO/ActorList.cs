﻿using Billboard.WCFService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Billboard.WCFService.DTO
{
    [Serializable()]
    public class ActorList
    {
        [XmlArray(""), XmlArrayItem(typeof(Actor), ElementName = "Actor")]
        public virtual List<Actor> Actors { get; set; }
    }
}
