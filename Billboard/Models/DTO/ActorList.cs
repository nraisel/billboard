﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Models.DTO
{
    [Serializable()]
    public class ActorList
    {
        public virtual List<Actor> Actors { get; set; }
    }
}
