﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Models.DTO
{
    public class ActorResponse
    {
        public string ErrorMessage { get; set; }
        public List<MyActorFromExternalWebApi> Actors { get; set; }
        public ActorResponse()
        {
            Actors = new List<MyActorFromExternalWebApi>();
        }
    }
}
