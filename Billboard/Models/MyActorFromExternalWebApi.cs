﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Models
{
    public class MyActorFromExternalWebApi
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string PhotoImg { get; set; }
        public List<Movie> Movies { get; set; }
        public List<Serial> Serials { get; set; }
    }
}
