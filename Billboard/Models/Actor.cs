﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Models
{
    [Serializable()]
    public class Actor
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string PhotoImg { get; set; }

        public Actor()
        {

        }
    }
}
