﻿using Billboard.Core.Common.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Models.Data_Layer.Contract
{
    public interface IActorRepository : IRepository<Actor>
    {
    }
}
