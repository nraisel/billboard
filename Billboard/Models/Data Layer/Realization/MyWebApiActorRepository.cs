﻿using Billboard.Core.Common;
using Billboard.Models.Data_Layer.Contract;
using Billboard.Models.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Models.Data_Layer.Realization
{
    public class MyWebApiActorRepository : IMyWebApiActorRepository
    {
        public MyActorFromExternalWebApi Add(MyActorFromExternalWebApi entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<MyActorFromExternalWebApi> Get()
        {
            var actorList = new List<MyActorFromExternalWebApi>();
            string uri = ConfigurationManager.AppSettings["BillboardCacheWebApiForMongoDbUrl - Actor"];
            //var result = HttpHelper.HttpGetJson<MyActorFromExternalWebApiList>(uri);
            var result = HttpHelper.HttpGetJson<List<MyActorFromExternalWebApi>>(uri);
            foreach (var actor in result)
            {
                var everyObjActor = new MyActorFromExternalWebApi
                {
                    Id = actor.Id,
                    Name = actor.Name,
                    PhotoImg = actor.PhotoImg
                };
                actorList.Add(everyObjActor);
            }
  
            return actorList.AsQueryable<MyActorFromExternalWebApi>();
        }

        public MyActorFromExternalWebApi GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public void Remove(MyActorFromExternalWebApi entity)
        {
            throw new NotImplementedException();
        }

        public MyActorFromExternalWebApi Update(MyActorFromExternalWebApi entity)
        {
            throw new NotImplementedException();
        }
    }
}
