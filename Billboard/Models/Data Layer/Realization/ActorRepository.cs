﻿using Billboard.Core.Common;
using Billboard.Core.Common.Contract;
using Billboard.Models.Data_Layer.Contract;
using Billboard.Models.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Billboard.Models.Data_Layer.Realization
{
    public class ActorRepository : IActorRepository
    {
        private BillboardProxyService.BillboardServiceClient _billboardServiceManager = new
                BillboardProxyService.BillboardServiceClient(ConfigurationManager.AppSettings["WcfBindingName"]);

        public Actor Add(Actor entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Actor> Get()
        {
            var response = _billboardServiceManager.GetActorsAsync();
            var result = SerializerHelper.XmlDeserializeFromString<ActorList>(response.Result);
            return result.Actors.AsQueryable();
        }

        public Actor GetById(string id)
        {
            var result = new HttpResponseMessage();
            using (HttpClient client = new HttpClient())
            {
                string uri = "http://localhost:49791/BillboardService.svc";
                result = client.GetAsync(uri).Result;
            }
            var obj = result.Content.ReadAsStringAsync().Result;
            var resObj = SerializerHelper.XmlDeserializeFromString<ActorList>(result.Content.ReadAsStringAsync().Result);
            return resObj.Actors.Where(ac => ac.Id == int.Parse(id)).FirstOrDefault();
        }

        public void Remove(string id)
        {
            throw new NotImplementedException();
        }

        public void Remove(Actor entity)
        {
            throw new NotImplementedException();
        }

        public Actor Update(Actor entity)
        {
            throw new NotImplementedException();
        }
    }
}
