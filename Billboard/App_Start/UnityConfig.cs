using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Billboard.Models.Data_Layer.Contract;
using Billboard.Models.Data_Layer.Realization;
using System.Web.Http;

namespace Billboard.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }

        private static void RegisterTypes(UnityContainer container)
        {
            container.RegisterType<IActorRepository, ActorRepository>();
            container.RegisterType<IMyWebApiActorRepository, MyWebApiActorRepository>();
        }
    }
}
