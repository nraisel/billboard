﻿using Billboard.Models.Data_Layer.Contract;
using Billboard.Models.Data_Layer.Realization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace Billboard.Controllers
{
    public class BillboardController : ApiController
    {
        private IActorRepository _actorRepository;
        //private BillboardServiceReference.BillboardServiceClient billboardServiceClient;
        public BillboardController(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
            //billboardServiceClient = new BillboardServiceReference.BillboardServiceClient();
        }
        // GET: Billboard

        [System.Web.Http.HttpGet]
        public IHttpActionResult Get()
        {
            try {
                var result = _actorRepository.Get();
                return Ok(JsonConvert.SerializeObject(result));
            }
            catch (Exception ex)
            {

                return Ok(ex.Message);
            }
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetById(string id)
        {
            try
            {
                var result = _actorRepository.GetById(id);
                return Ok(JsonConvert.SerializeObject(result));
            }
            catch (Exception ex)
            {

                return Ok(ex.Message);
            }
        }
    }
}