﻿using Billboard.Models.Data_Layer.Contract;
using Billboard.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Billboard.Controllers
{
    public class ActorWebApiController : ApiController
    {
        private IMyWebApiActorRepository _myWebApiActorRepository;
        public ActorWebApiController(IMyWebApiActorRepository myWebApiActorRepository)
        {
            _myWebApiActorRepository = myWebApiActorRepository;
        }
        // GET: ActorWebApi
        public IHttpActionResult Get()
        {
            try
            {
                var result = _myWebApiActorRepository.Get();
                if (result == null)
                    return BadRequest();
                else
                {
                    var response = new ActorResponse();
                    response.Actors = result.ToList();
                    response.ErrorMessage = null;
                    return Ok(response);
                }
                    
            }
            catch (Exception ex)
            {
                var response = new ActorResponse();
                response.Actors = null;
                response.ErrorMessage = ex.Message + " - " + ex.InnerException + " - " + ex.InnerException.Message;
                return Ok(response);
            }
        }
    }
}