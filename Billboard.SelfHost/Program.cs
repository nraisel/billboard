﻿using Billboard.WCFService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.SelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            try {
                ServiceHost host = new ServiceHost(typeof(BillboardService));
                host.Open();
                Console.WriteLine("Service started and hosted in this console.");
                Console.ReadKey();
                host.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
