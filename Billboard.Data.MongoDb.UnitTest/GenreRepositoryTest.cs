﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System.Collections.Generic;
using MongoDB.Bson;

namespace Billboard.Data.MongoDb.UnitTest
{
    [TestClass]
    public class GenreRepositoryTest
    {
        [TestMethod]
        public void GetAllGenresTest()
        {
            MongoRepository<Genre> genreRepository = new MongoRepository<Genre>();
            var collection = genreRepository.Count();
            Assert.AreEqual(8, collection);
        }

        [TestMethod]
        public void AddGenreTest()
        {
            var premierDate = new BsonDateTime(DateTime.Now);
            var video1 = new Movie
            {
                Name = "Solo en casa",
                Duration = "2:00:00",
                Price = 5,
                Sinampis = "",
                PremiereDate = premierDate
            };
            var video2 = new VideoHD
            {
                Name = "Solo en casa 2",
                Duration = "1:54:26",
                Price = 5,
                Sinampis = "",
                PremiereDate = premierDate
            };
            var videos = new List<VideoHD>();
            videos.Add(video1);
            videos.Add(video2);
            var genre = new Genre { Name = "Comedy" };
            MongoRepository<Genre> genreRepository = new MongoRepository<Genre>();
            var result = genreRepository.Add(genre);
            Assert.AreEqual("8", result.Id);

        }

        [TestMethod]
        public void DeleteAllGenresTest()
        {
            MongoRepository<Genre> genreRepository = new MongoRepository<Genre>();
            genreRepository.DeleteAll();
            var result = genreRepository.Count();
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void GetGenreByIdTest()
        {
            MongoRepository<Genre> genreRepository = new MongoRepository<Genre>();
            var genre = genreRepository.GetById("5999089471053742ecc58a4a");
            Assert.IsNotNull(genre);
            Assert.AreEqual("Comedy", genre.Name);
        }

        [TestMethod]
        public void UpdateGenreTest()
        {
            MongoRepository<Genre> genreRepository = new MongoRepository<Genre>();
            var genre = genreRepository.GetById("5999089471053742ecc58a4a");
            genre.Name = "Comedy";
            var result = genreRepository.Update(genre);
            Assert.AreEqual("Comedy", result.Name);
        }
    }
}
