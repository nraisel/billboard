﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billboard.Data.MongoDb.Contract;
using Billboard.Business.Entities.MongoDb.Model;

namespace Billboard.Data.MongoDb.UnitTest
{
    /// <summary>
    /// Summary description for ActorRepositoryTest
    /// </summary>
    [TestClass]
    public class ActorRepositoryTest
    {
        [TestMethod]
        public void GetActorsCountTest()
        {
            IActorRepository actorRepository = new ActorRepository();
            var collection = actorRepository.Count();
            Assert.AreEqual(8, collection);
        }

        [TestMethod]
        public void AddActorTest()
        {
            IActorRepository actorRepository = new ActorRepository();
            var actor = new Actor { Name = "Guille de Noguera", PhotoImg = "", Movies = null, Serials = null };
            var result = actorRepository.Add(actor);
            Assert.AreEqual("Raisel", result.Name.Split(' ')[0]);
        }

        [TestMethod]
        public void DeleteActorTest()
        {
            IActorRepository actorRepository = new ActorRepository();
            string id = "599cf75271053749d0a23978";
            actorRepository.Delete(id);
            var count = actorRepository.Count();
            Assert.AreEqual(0, count);
        }
    }
}
