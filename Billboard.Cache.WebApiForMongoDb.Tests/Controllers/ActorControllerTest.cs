﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billboard.Cache.WebApiForMongoDb.Controllers;
using Billboard.Cache.WebApiForMongoDb.Infrastructure.Realization;
using Billboard.Data.MongoDb.Contract;
using Billboard.Data.MongoDb;
using Billboard.Cache.WebApiForMongoDb.Infrastructure.Contract;
using System.Linq;
using Billboard.Cache.WebApiForMongoDb.Models;
using System.Threading.Tasks;

namespace Billboard.Cache.WebApiForMongoDb.Tests.Controllers
{
    /// <summary>
    /// Summary description for ActorControllerTest
    /// </summary>
    [TestClass]
    public class ActorControllerTest
    {
        [TestMethod]
        public void GetActorsTest()
        {
            // Arrange
            IActorRepository _mongoDbActorRepository = new ActorRepository();
            IMyActorRepository actorRepository = new MyActorRepository(_mongoDbActorRepository);
            ActorController controller = new ActorController(actorRepository);
            var result = controller.Get();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetActorsThroughRepositoryTest()
        {
            // Arrange
            IActorRepository _mongoDbActorRepository = new ActorRepository();
            IMyActorRepository actorRepository = new MyActorRepository(_mongoDbActorRepository);
            var result = actorRepository.Get();
            Assert.IsNotNull(result);
            Assert.AreEqual(7, result.Count());
            
        }

        [TestMethod]
        public void AddActorTest()
        {
            // Arrange
            var actor = new Actor
            {
                Name = "Tu y Yo",
                PhotoImg = "kk",
                Movies = null,
                Serials = null
            };
            IActorRepository _mongoDbActorRepository = new ActorRepository();
            IMyActorRepository actorRepository = new MyActorRepository(_mongoDbActorRepository);
            ActorController controller = new ActorController(actorRepository);
            var result = controller.AddActor(actor);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddActorThroughRepositoryTest()
        {
            // Arrange
            var actor = new Actor {
                Name = "Yo",
                PhotoImg = "kk",
                Movies = null,
                Serials = null
            };
            IActorRepository _mongoDbActorRepository = new ActorRepository();
            IMyActorRepository actorRepository = new MyActorRepository(_mongoDbActorRepository);
            var result = actorRepository.Add(actor);
            Assert.IsNotNull(result);
            Assert.AreEqual("Yo", result.Name);

        }
    }
}
