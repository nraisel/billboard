﻿using Billboard.Business.Entities;
using Billboard.Core.Common.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.Contract
{
    public interface IActorRepository : IRepository<Actor>
    {
        IList<Actor> getActorsPerSerial(int id);
        IList<Actor> getActorsPerMovie(int id);

    }
}
