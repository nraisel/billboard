﻿using Billboard.Business.Entities;
using Billboard.Core.Common.Contract;
using Billboard.Data.Billboard.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.Contract
{
    public interface IBillboardRepository : IRepository<BillboardSchedule>
    {
        IList<BillboardSchedule> getBillboard(DateTime date);
    }
}
