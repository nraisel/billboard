﻿using Billboard.Business.Entities;
using Billboard.Core.Common.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.Contract
{
    public interface ISerialRepository : IRepository<Serial>
    {
        IList<Serial> getSerialslByName(string name);
        IList<Serial> getSerialslByGenre(string genre);
    }
}
