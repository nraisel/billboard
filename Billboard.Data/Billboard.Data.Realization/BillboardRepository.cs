﻿using Billboard.Data.Billboard.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billboard.Business.Entities;
using Billboard.Core.Common;
using Billboard.Data.Billboard.Data.Context;
using Billboard.Data.Billboard.Data.DTO;
using System.Data.Entity;

namespace Billboard.Data.Billboard.Data.Realization
{
    public class BillboardRepository : BaseRepository<BillboardSchedule, BillboardContext>, IBillboardRepository
    {

        protected override IQueryable<BillboardSchedule> GetEntities(BillboardContext context)
        {
            var movies = context.Movies.ToList();
            var mov = context.VideoHDs.ToList();
            var serials = context.Serials.ToListAsync();
            var result = new List<BillboardSchedule>();
            result.Add(new BillboardSchedule { MoviesInBillboard = movies, SerialsInBillboard = serials.Result });
            return result.AsQueryable();
        }
        protected override BillboardSchedule GetEntity(BillboardContext context, int id)
        {
            return null;
        }

        public IList<BillboardSchedule> getBillboard(DateTime date)
        {
            using (var context = new BillboardContext())
            {
                var result = new List<BillboardSchedule>();
                var movieList = new List<Movie>();
                var serialList = new List<Serial>();
                var billboardList = GetEntities(context).ToList();
                //return billboardList.AsEnumerable().ToList();
                System.Threading.Tasks.Task[] tasks = new System.Threading.Tasks.Task[]
                    {
                        System.Threading.Tasks.Task.Factory.StartNew(() => {
                            billboardList.ForEach(m => {
                                movieList = m.MoviesInBillboard
                                             .Where(mov => mov.PremiereDate.ToShortDateString() == date.ToShortDateString())
                                             .ToList();
                            });
                        }),
                        System.Threading.Tasks.Task.Factory.StartNew(() => {
                            billboardList.ForEach(s => {
                                serialList = s.SerialsInBillboard
                                              .Where(ser => ser.PremiereDate.ToShortDateString() == date.ToShortDateString())
                                              .ToList();
                            });
                        })
                    };
                System.Threading.Tasks.Task.WaitAll(tasks);
                result.Add(new BillboardSchedule { MoviesInBillboard = movieList, SerialsInBillboard = serialList });
                return result;
            }
        }
    }
}
