﻿using Billboard.Business.Entities;
using Billboard.Core.Common;
using Billboard.Data.Billboard.Data.Context;
using Billboard.Data.Billboard.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.Realization
{
    public class SeasonRepository : BaseRepository<Season, BillboardContext>, ISeasonRepository
    {
        public IList<Season> getSeasonsBySerialName(string serialName)
        {
            using (var context = new BillboardContext())
            {
                return context.Seasons.Where(s => s.Serial.Name.ToLower() == serialName.ToLower()).ToList();
            }
        }

        protected override IQueryable<Season> GetEntities(BillboardContext context)
        {
            return context.Seasons.AsQueryable();
        }

        protected override Season GetEntity(BillboardContext context, int id)
        {
            return context.Seasons.Where(s => s.Id == id).FirstOrDefault();
        }
    }
}
