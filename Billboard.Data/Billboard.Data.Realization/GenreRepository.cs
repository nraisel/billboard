﻿using Billboard.Business.Entities;
using Billboard.Core.Common;
using Billboard.Data.Billboard.Data.Context;
using Billboard.Data.Billboard.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.Realization
{
    public class GenreRepository : BaseRepository<Genre, BillboardContext>, IGenreRepository
    {
        protected override IQueryable<Genre> GetEntities(BillboardContext context)
        {
            return context.Genres.AsQueryable();
        }

        protected override Genre GetEntity(BillboardContext context, int id)
        {
            return context.Genres.Where(g => g.Id == id).FirstOrDefault();
        }
    }
}
