﻿using Billboard.Business.Entities;
using Billboard.Core.Common;
using Billboard.Data.Billboard.Data.Context;
using Billboard.Data.Billboard.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.Realization
{
    public class MovieRepository : BaseRepository<Movie, BillboardContext>, IMovieRepository
    {
        public IList<Movie> getMoviesByGenre(string genre)
        {
            using (var context = new BillboardContext())
            {
                return context.Movies.Where(m => m.Genre.Name == genre).ToList();
            }
        }

        protected override IQueryable<Movie> GetEntities(BillboardContext context)
        {
            return context.Movies.AsQueryable();
        }

        protected override Movie GetEntity(BillboardContext context, int id)
        {
            return context.Movies.Where(m => m.Id == id).FirstOrDefault();
        }
    }
}
