﻿using Billboard.Business.Entities;
using Billboard.Core.Common;
using Billboard.Data.Billboard.Data.Context;
using Billboard.Data.Billboard.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Billboard.Data.Billboard.Data.Realization
{
    public class ActorRepository : BaseRepository<Actor, BillboardContext>, IActorRepository
    {
        public IList<Actor> getActorsPerMovie(int id)
        {
            using (var context = new BillboardContext())
            {
                var movie = context.Movies.Where(s => s.Id == id).FirstOrDefault();
                return context.Actors.Where(ac => ac.VideoHDs.Contains(movie)).ToList();
            }
        }

        public IList<Actor> getActorsPerSerial(int id)
        {
            using (var context = new BillboardContext())
            {
                var serial = context.Serials.Where(s=>s.Id == id).FirstOrDefault();
                return context.Actors.Where(ac => ac.VideoHDs.Contains(serial)).ToList();
            }
        }

        protected override IQueryable<Actor> GetEntities(BillboardContext context)
        {
            return context.Actors;
        }

        protected override Actor GetEntity(BillboardContext context, int id)
        {
            return context.Actors.Where(ac => ac.Id == id).FirstOrDefault();
        }
    }
}
