﻿using Billboard.Business.Entities;
using Billboard.Core.Common;
using Billboard.Data.Billboard.Data.Context;
using Billboard.Data.Billboard.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.Realization
{
    public class SerialRepository : BaseRepository<Serial, BillboardContext>, ISerialRepository
    {
        public IList<Serial> getSerialslByGenre(string genre)
        {
            using (var context = new BillboardContext())
            {
                return context.Serials.Where(s => s.Genre.Name == genre).ToList();
            }
        }

        public IList<Serial> getSerialslByName(string name)
        {
            using (var context = new BillboardContext())
            {
                return context.Serials.Where(s => s.Name.Contains(name)).ToList();
            }
        }

        protected override IQueryable<Serial> GetEntities(BillboardContext context)
        {
            return context.Serials.AsQueryable();
        }

        protected override Serial GetEntity(BillboardContext context, int id)
        {
            return context.Serials.Where(s => s.Id == id).FirstOrDefault();
        }
    }
}
