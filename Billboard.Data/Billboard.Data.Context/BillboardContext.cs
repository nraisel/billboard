﻿using Billboard.Business.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.Context
{
    public class BillboardContext : DbContext
    {
        public BillboardContext()
            : base("BillboardContext")
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }

        public DbSet<VideoHD> VideoHDs { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Serial> Serials { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Episode> Episodes { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Client> Clients { get; set; }

        public static BillboardContext Create()
        {
            return new BillboardContext();
        }
    }
}
