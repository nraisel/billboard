namespace Billboard.Data.Migrations
{
    using Billboard.Data.Context;
    using Business.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BillboardContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BillboardContext context)
        {
            var filmList = new List<Movie>();
            var actorList1 = new List<Actor>();
            var actorList2 = new List<Actor>();

            var actor1 = new Actor { Id = 1, Name = "Jon", PhotoImg = "" };
            var actor2 = new Actor { Id = 2, Name = "Matt", PhotoImg = "" };
            var actor3 = new Actor { Id = 3, Name = "Ray", PhotoImg = "" };
            var actor4 = new Actor { Id = 4, Name = "Susan", PhotoImg = "" };
            actorList1.Add(actor1);
            actorList1.Add(actor2);
            actorList1.Add(actor3);
            actorList1.Add(actor4);

            actorList2.Add(actor4);
            actorList2.Add(actor3);

            var film1 = new Movie
            {
                Id = 1,
                Name = "Galaxy war",
                Duration = "2hrs : 24 min",
                PremiereDate = System.DateTime.Now,
                Price = 18,
                Actors = actorList1
            };
            var film2 = new Movie
            {
                Id = 2,
                Name = "Forest Gump",
                Duration = "3hrs : 01 min",
                PremiereDate = System.DateTime.Now.AddDays(1),
                Price = 13,
                Actors = actorList2
            };

            var episode1 = new Episode
            {
                Id = 1,
                Name = "E1: El Chema",
                Duration = "45 min",
                Description = "description E1"
            };

            var episode2 = new Episode
            {
                Id = 2,
                Name = "E2: El Chema",
                Duration = "48 min",
                Description = "description E1"
            };

            var episode3 = new Episode
            {
                Id = 3,
                Name = "E3: El Chema",
                Duration = "46:23 min",
                Description = "description E1"
            };

            var episodesList = new List<Episode>();
            episodesList.Add(episode1);
            episodesList.Add(episode2);
            episodesList.Add(episode3);

            var season1 = new Season
            {
                Id = 1,
                Episodes = episodesList,
                NumberOfEpisodes = 80
            };

            var seasonList = new List<Season>();
            seasonList.Add(season1);

            var serial = new Serial
            {
                Id = 1,
                Name = "Chema",
                Duration = "1 Season",
                PremiereDate = DateTime.Now,
                CoverImg = "",
                Actors = actorList1,
                Sinampis = "La historia del Chema",
                Seasons = seasonList
            };

            context.Actors.AddOrUpdate(
                a => a.Id,
                actor1,
                actor2,
                actor3,
                actor4);

            context.Movies.AddOrUpdate(
                m => m.Id,
                film1,
                film2);

            context.Serials.AddOrUpdate(
                s => s.Id,
                serial);
        }
    }
}
