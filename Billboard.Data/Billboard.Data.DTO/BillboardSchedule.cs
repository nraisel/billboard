﻿using Billboard.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Billboard.Data.DTO
{
    public class BillboardSchedule
    {
        public virtual IList<Movie> MoviesInBillboard { get; set; }
        public virtual IList<Serial> SerialsInBillboard { get; set; }
    }
}
