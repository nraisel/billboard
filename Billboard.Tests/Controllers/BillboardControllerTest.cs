﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billboard.Controllers;
using Billboard.Models.Data_Layer.Realization;
using Billboard.Models.Data_Layer.Contract;

namespace Billboard.Tests.Controllers
{
    /// <summary>
    /// Summary description for BillboardControllerTest
    /// </summary>
    [TestClass]
    public class BillboardControllerTest
    {

        [TestMethod]
        public void GetActorsUsingHttpClient()
        {
            // Arrange
            IActorRepository repo = new ActorRepository();
            BillboardController controller = new BillboardController(repo);
            var result = controller.GetById("5");
            Assert.IsNotNull(result);
        }
    }
}
