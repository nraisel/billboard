﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billboard.Models.Data_Layer.Contract;
using Billboard.Models.Data_Layer.Realization;
using System.Linq;

namespace Billboard.Tests.Controllers
{
    /// <summary>
    /// Summary description for ActorWebApiControllerTest
    /// </summary>
    [TestClass]
    public class ActorWebApiControllerTest
    {
        [TestMethod]
        public void GetAllActorUsingHttpClientDirecltyFromRepositoryTest()
        {
            IMyWebApiActorRepository repo = new MyWebApiActorRepository();
            var result = repo.Get();
            Assert.IsNotNull(result);
            Assert.AreEqual(9, result.Count());
        }
    }
}
