using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Billboard.Core.Common
{
    public static class HttpHelper
    {
        public static T HttpGetJson<T>(string uri)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromMilliseconds(Convert.ToInt32(ConfigurationManager.AppSettings["APITimeOut"]));
                    response = httpClient.GetAsync(uri).Result;
                }
                return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static T HttpGet<T>(string uri)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromMilliseconds(Convert.ToInt32(ConfigurationManager.AppSettings["APITimeOut"]));
                    var responseFromURL = httpClient.GetAsync(uri).Result;
                    response = responseFromURL;
                }

                var serializer = new XmlSerializer(typeof(T));

                return (T)serializer.Deserialize(response.Content.ReadAsStreamAsync().Result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
