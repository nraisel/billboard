﻿using Billboard.Core.Common.Contract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Core.Common
{
    public abstract class BaseRepository<T, U> : IRepository<T>
        where T : class, new()
        where U : DbContext, new()
    {
        protected virtual T AddEntity(U context, T entity)
        {
            context.Entry(entity).State = EntityState.Added;
            return entity;
        }

        protected virtual T UpdateEntity(U context, T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        protected abstract IQueryable<T> GetEntities(U context);

        protected abstract T GetEntity(U context, int id);

        public T Update(T entity)
        {
            using (U context = new U())
            {
                T existingEntity = UpdateEntity(context, entity);

                context.SaveChanges();
                return existingEntity;
            }
        }

        public void Remove(T entity)
        {
            using (U context = new U())
            {
                context.Entry<T>(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Remove(string id)
        {
            using (U context = new U())
            {
                T entity = GetEntity(context, int.Parse(id));
                context.Entry<T>(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public IQueryable<T> Get()
        {
            using (U context = new U())
            {
                return (GetEntities(context)).ToArray().ToList().AsQueryable();
            }
        }

        public T GetById(string id)
        {
            using (U context = new U())
            {
                return GetEntity(context, int.Parse(id));
            }
        }

        public T Add(T entity)
        {
            using (U context = new U())
            {
                T addedEntity = AddEntity(context, entity);
                context.SaveChanges();
                return addedEntity;
            }
        }
    }
}
