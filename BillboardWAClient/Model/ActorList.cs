﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillboardWAClient.Model
{
    [Serializable()]
    public class ActorList
    {
        public virtual List<Actor> Actors { get; set; }
    }
}
