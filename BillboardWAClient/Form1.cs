﻿using Billboard.Core.Common;
using BillboardWAClient.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BillboardWAClient
{
    public partial class Form1 : Form
    {
        static HttpClient client = new HttpClient();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var res = new HttpResponseMessage();

                //client.BaseAddress = new Uri("http://localhost:49791/");
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));


                var uri = new Uri("http://localhost:49791/BillboardService.svc");
                res = client.GetAsync(uri).Result;
                var obj = res.Content.ReadAsStringAsync().Result;

                BillboardProxyService.BillboardServiceClient clientBillboardProxyService =
                    new BillboardProxyService.BillboardServiceClient("NetTcpBinding_IBillboardService");
                MessageBox.Show(clientBillboardProxyService.Endpoint.Address.ToString()); 
                string result = clientBillboardProxyService.GetActorsAsync().Result;
                var actors = SerializerHelper.XmlDeserializeFromString<ActorList>(result);
                textBox1.Text = actors.Actors.FirstOrDefault().Name;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }
    }
}
