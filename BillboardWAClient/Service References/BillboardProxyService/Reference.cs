﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BillboardWAClient.BillboardProxyService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="BillboardProxyService.IBillboardService")]
    public interface IBillboardService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IBillboardService/GetActors", ReplyAction="http://tempuri.org/IBillboardService/GetActorsResponse")]
        string GetActors();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IBillboardService/GetActors", ReplyAction="http://tempuri.org/IBillboardService/GetActorsResponse")]
        System.Threading.Tasks.Task<string> GetActorsAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IBillboardServiceChannel : BillboardWAClient.BillboardProxyService.IBillboardService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class BillboardServiceClient : System.ServiceModel.ClientBase<BillboardWAClient.BillboardProxyService.IBillboardService>, BillboardWAClient.BillboardProxyService.IBillboardService {
        
        public BillboardServiceClient() {
        }
        
        public BillboardServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public BillboardServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BillboardServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BillboardServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetActors() {
            return base.Channel.GetActors();
        }
        
        public System.Threading.Tasks.Task<string> GetActorsAsync() {
            return base.Channel.GetActorsAsync();
        }
    }
}
