﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Core.Common.Contract
{
    public interface IRepository<T>
    {
        IQueryable<T> Get();
        T GetById(string id);
        T Add(T entity);
        T Update(T entity);
        void Remove(T entity);
        void Remove(string id);
    }
}
