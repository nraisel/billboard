﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Business.Entities
{
    public class VideoHD
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Sinampis { get; set; }
        public virtual string Duration { get; set; }
        public virtual double Price { get; set; }
        public virtual string CoverImg { get; set; }
        public virtual DateTime PremiereDate { get; set; }
        public virtual IList<Actor> Actors { get; set; }
        public virtual Client Client { get; set; }
        public virtual Genre Genre { get; set; }
    }
}
