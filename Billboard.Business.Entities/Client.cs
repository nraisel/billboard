﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Business.Entities
{
    public class Client
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual string estado { get; set; }
        public virtual IList<VideoHD> VideoHDs { get; set; }
    }
}
