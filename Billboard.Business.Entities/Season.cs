﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Business.Entities
{
    public class Season
    {
        public virtual int Id { get; set; }
        public virtual int NumberOfEpisodes { get; set; }
        public virtual Serial Serial { get; set; }
        public virtual IList<Episode> Episodes { get; set; }
    }
}
