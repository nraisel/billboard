﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Billboard.Data.Billboard.Data.Realization;
using System.Collections.Generic;
using Billboard.Data.Billboard.Data.DTO;
using System.Data.Entity;

namespace Billboard.Data.Test
{
    [TestClass]
    public class BillboardTest
    {
        [TestMethod]
        public void TestToGetBillboard()
        {
        }

        [TestMethod]
        public void TestToGetBillboardByDate()
        {
            DateTime date = System.DateTime.Now;
            var billboardRepository = new BillboardRepository();
            var result = billboardRepository.getBillboard(date);
            Assert.AreEqual(1, result.Count);
        }
    }
}
