﻿using Billboard.Data.Billboard.Data.Realization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.Test
{
    [TestClass]
    public class ActorRepositoryTest
    {
        [TestMethod]
        public void TestToGetAllActors()
        {
            var actorRepository = new ActorRepository();
            var result = actorRepository.Get();
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count());
        }

    }
}
