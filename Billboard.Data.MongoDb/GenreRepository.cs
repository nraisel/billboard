﻿using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Collections;
using System.Linq.Expressions;

namespace Billboard.Data.MongoDb
{
    public class GenreRepository : IRepository<Genre>
    {
        private MongoRepository<Genre> _genreRepository;

        public GenreRepository()
        {
            _genreRepository = new MongoRepository<Genre>(ConfigurationManager.AppSettings["MongoServerSettings"]);
        }

        public MongoCollection<Genre> Collection
        {
            get
            {
                return _genreRepository.Collection;
            }
        }

        public IEnumerator<Genre> GetEnumerator()
        {
            return Collection.FindAll().GetEnumerator();
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Genre> entities)
        {
            foreach (var item in entities)
                _genreRepository.Add(item);
        }

        public Genre Add(Genre entity)
        {
            return _genreRepository.Add(entity);
        }

        public long Count()
        {
            return Collection.Count();
        }

        public void Delete(Expression<Func<Genre, bool>> predicate)
        {
            var genre = _genreRepository.Where(predicate).FirstOrDefault();
            Delete(genre);
        }

        public void Delete(Genre entity)
        {
            _genreRepository.Delete(entity);
        }

        public void Delete(string id)
        {
            var genre = GetById(id);
            Delete(genre);
        }

        public void DeleteAll()
        {
            _genreRepository.DeleteAll();
        }

        public bool Exists(Expression<Func<Genre, bool>> predicate)
        {
            return _genreRepository.Exists(predicate);
        }

        public Genre GetById(string id)
        {
            return _genreRepository.GetById(id);
        }

        

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Genre> entities)
        {
            foreach (var item in entities)
                Update(item);
        }

        public Genre Update(Genre entity)
        {
            return _genreRepository.Update(entity);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
