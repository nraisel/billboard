﻿using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Data.MongoDb.Contract
{
    public interface IActorRepository : IRepository<Actor>
    {
    }
}
