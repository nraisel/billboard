﻿using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Collections;
using System.Linq.Expressions;
using System.Configuration;

namespace Billboard.Data.MongoDb
{
    public class EpisodeRepository : IRepository<Episode>
    {
        private MongoRepository<Episode> _episodeRepository;
        public EpisodeRepository()
        {
            _episodeRepository = new MongoRepository<Episode>(ConfigurationManager.AppSettings["MongoServerSettings"]);
        }
        public MongoCollection<Episode> Collection
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Episode> entities)
        {
            foreach (var item in entities)
                Add(item);
        }

        public Episode Add(Episode entity)
        {
            return _episodeRepository.Add(entity);
        }

        public long Count()
        {
            return _episodeRepository.Count();
        }

        public void Delete(Episode entity)
        {
            _episodeRepository.DefaultIfEmpty(entity);
        }

        public void Delete(Expression<Func<Episode, bool>> predicate)
        {
            _episodeRepository.Delete(predicate);
        }

        public void Delete(string id)
        {
            _episodeRepository.Delete(id);
        }

        public void DeleteAll()
        {
            _episodeRepository.DeleteAll();
        }

        public bool Exists(Expression<Func<Episode, bool>> predicate)
        {
            return _episodeRepository.Exists(predicate);
        }

        public Episode GetById(string id)
        {
            return _episodeRepository.GetById(id);
        }

        public IEnumerator<Episode> GetEnumerator()
        {
            return _episodeRepository.ToList<Episode>().GetEnumerator();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Episode> entities)
        {
            foreach (var item in entities)
                Update(item);
        }

        public Episode Update(Episode entity)
        {
            return _episodeRepository.Update(entity);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _episodeRepository.ToList().GetEnumerator();
        }
    }
}
