﻿using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Collections;
using System.Linq.Expressions;
using System.Configuration;

namespace Billboard.Data.MongoDb
{
    public class MovieRepository : IRepository<Movie>
    {
        private MongoRepository<Movie> _repository;
        public MovieRepository()
        {
            _repository = new MongoRepository<Movie>(ConfigurationManager.AppSettings["MongoServerSettings"]);
        }
        public MongoCollection<Movie> Collection
        {
            get
            {
                return _repository.Collection;
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Movie> entities)
        {
            foreach (var item in entities)
                Add(item);
        }

        public Movie Add(Movie entity)
        {
            return _repository.Add(entity);
        }

        public long Count()
        {
            return _repository.Count();
        }

        public void Delete(Movie entity)
        {
            _repository.Delete(entity);
        }

        public void Delete(Expression<Func<Movie, bool>> predicate)
        {
            _repository.Delete(predicate);
        }

        public void Delete(string id)
        {
            _repository.Delete(id);
        }

        public void DeleteAll()
        {
            _repository.DeleteAll();
        }

        public bool Exists(Expression<Func<Movie, bool>> predicate)
        {
            return _repository.Exists(predicate);
        }

        public Movie GetById(string id)
        {
            return _repository.GetById(id);
        }

        public IEnumerator<Movie> GetEnumerator()
        {
            return _repository.ToList<Movie>().GetEnumerator();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Movie> entities)
        {
            foreach (var item in entities)
                Update(item);
        }

        public Movie Update(Movie entity)
        {
            return _repository.Update(entity);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _repository.ToList().GetEnumerator();
        }
    }
}
