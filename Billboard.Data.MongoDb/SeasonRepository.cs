﻿using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Collections;
using System.Linq.Expressions;
using System.Configuration;

namespace Billboard.Data.MongoDb
{
    public class SeasonRepository : IRepository<Season>
    {
        private MongoRepository<Season> _seasonRepository;
        public SeasonRepository()
        {
            _seasonRepository = new MongoRepository<Season>(ConfigurationManager.AppSettings["MongoServerSettings"]);
        }
        public MongoCollection<Season> Collection
        {
            get
            {
                return _seasonRepository.Collection;
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Season> entities)
        {
            foreach (var item in entities)
                Add(item);
        }

        public Season Add(Season entity)
        {
            return _seasonRepository.Add(entity);
        }

        public long Count()
        {
            return _seasonRepository.Count();
        }

        public void Delete(Season entity)
        {
            _seasonRepository.Delete(entity);
        }

        public void Delete(Expression<Func<Season, bool>> predicate)
        {
            _seasonRepository.Delete(predicate);
        }

        public void Delete(string id)
        {
            _seasonRepository.Delete(id);
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public bool Exists(Expression<Func<Season, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Season GetById(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<Season> GetEnumerator()
        {
            return _seasonRepository.ToList<Season>().GetEnumerator();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Season> entities)
        {
            foreach (var item in entities)
                Update(item);
        }

        public Season Update(Season entity)
        {
            return _seasonRepository.Update(entity);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _seasonRepository.ToList().GetEnumerator();
        }
    }
}
