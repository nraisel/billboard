﻿using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Collections;
using System.Linq.Expressions;
using System.Configuration;

namespace Billboard.Data.MongoDb
{
    public class ClientRepository : IRepository<Client>
    {
        private MongoRepository<Client> _repository;
        public ClientRepository()
        {
            _repository = new MongoRepository<Client>(ConfigurationManager.AppSettings["MongoServerSettings"]);
        }
        public MongoCollection<Client> Collection
        {
            get
            {
                return _repository.Collection;
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Client> entities)
        {
            foreach (var item in entities)
                Add(item);
        }

        public Client Add(Client entity)
        {
            return _repository.Add(entity);
        }

        public long Count()
        {
            return _repository.Count();
        }

        public void Delete(Client entity)
        {
            _repository.Delete(entity);
        }

        public void Delete(Expression<Func<Client, bool>> predicate)
        {
            _repository.Delete(predicate);
        }

        public void Delete(string id)
        {
            _repository.Delete(id);
        }

        public void DeleteAll()
        {
            _repository.DeleteAll();
        }

        public bool Exists(Expression<Func<Client, bool>> predicate)
        {
            return _repository.Exists(predicate);
        }

        public Client GetById(string id)
        {
            return _repository.GetById(id);
        }

        public IEnumerator<Client> GetEnumerator()
        {
            return _repository.ToList<Client>().GetEnumerator();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Client> entities)
        {
            foreach (var item in entities)
                Update(item);
        }

        public Client Update(Client entity)
        {
            return _repository.Update(entity);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _repository.ToList().GetEnumerator();
        }
    }
}
