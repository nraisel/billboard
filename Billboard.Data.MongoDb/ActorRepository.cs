﻿using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Collections;
using System.Linq.Expressions;
using System.Configuration;
using Billboard.Data.MongoDb.Contract;

namespace Billboard.Data.MongoDb
{
    public class ActorRepository : IActorRepository//IRepository<Actor>
    {
        private MongoRepository<Actor> _actorRepository;
        public ActorRepository()
        {
            string cnnstring = ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString;
            _actorRepository = new MongoRepository<Actor>(cnnstring);
        }
        public MongoCollection<Actor> Collection
        {
            get
            {
                return _actorRepository.Collection;
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Actor> entities)
        {
            foreach (var item in entities)
                Add(item);
        }

        public Actor Add(Actor entity)
        {
            return _actorRepository.Add(entity);
        }

        public long Count()
        {
            return _actorRepository.Count();
        }

        public void Delete(Actor entity)
        {
            _actorRepository.Delete(entity);
        }

        public void Delete(Expression<Func<Actor, bool>> predicate)
        {
            _actorRepository.Delete(predicate);
        }

        public void Delete(string id)
        {
            _actorRepository.Delete(id);
        }

        public void DeleteAll()
        {
            _actorRepository.DeleteAll();
        }

        public bool Exists(Expression<Func<Actor, bool>> predicate)
        {
            return _actorRepository.Exists(predicate);
        }

        public Actor GetById(string id)
        {
            return _actorRepository.GetById(id);
        }

        public IEnumerator<Actor> GetEnumerator()
        {
            return _actorRepository.ToList<Actor>().GetEnumerator();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Actor> entities)
        {
            foreach (var item in entities)
                Update(item);
        }

        public Actor Update(Actor entity)
        {
            return _actorRepository.Update(entity);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _actorRepository.ToList().GetEnumerator();
        }
    }
}
