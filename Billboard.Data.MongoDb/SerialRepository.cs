﻿using Billboard.Business.Entities.MongoDb.Model;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using System.Collections;
using System.Linq.Expressions;
using System.Configuration;

namespace Billboard.Data.MongoDb
{
    public class SerialRepository : IRepository<Serial>
    {
        private MongoRepository<Serial> _serialRepository;
        public SerialRepository()
        {
            _serialRepository = new MongoRepository<Serial>(ConfigurationManager.AppSettings["MongoServerSettings"]);
        }
        public MongoCollection<Serial> Collection
        {
            get
            {
                return _serialRepository.Collection;
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Serial> entities)
        {
            foreach (var item in entities)
                Add(item);
        }

        public Serial Add(Serial entity)
        {
            return _serialRepository.Add(entity);
        }

        public long Count()
        {
            return _serialRepository.Count();
        }

        public void Delete(Serial entity)
        {
            _serialRepository.Delete(entity);
        }

        public void Delete(Expression<Func<Serial, bool>> predicate)
        {
            _serialRepository.Delete(predicate);
        }

        public void Delete(string id)
        {
            _serialRepository.Delete(id);
        }

        public void DeleteAll()
        {
            _serialRepository.DeleteAll();
        }

        public bool Exists(Expression<Func<Serial, bool>> predicate)
        {
            return _serialRepository.Exists(predicate);
        }

        public Serial GetById(string id)
        {
            return _serialRepository.GetById(id);
        }

        public IEnumerator<Serial> GetEnumerator()
        {
            return _serialRepository.ToList<Serial>().GetEnumerator();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Serial> entities)
        {
            foreach (var item in entities)
                Update(item);
        }

        public Serial Update(Serial entity)
        {
            return _serialRepository.Update(entity);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _serialRepository.ToList().GetEnumerator();
        }
    }
}
