﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public class Movie : VideoHD
    {
        public IList<Actor> Actors { get; set; }
        public Client Client { get; set; }
        public Genre Genre { get; set; }

        public Movie()
        {
            Actors = new List<Actor>();
        }
    }
}
