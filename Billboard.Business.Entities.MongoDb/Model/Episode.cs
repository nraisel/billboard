﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoRepository;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public class Episode : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Duration { get; set; }
    }
}
