﻿using MongoDB.Bson;
using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public class VideoHD : Entity
    {
        public string Name { get; set; }
        public string Sinampis { get; set; }
        public string Duration { get; set; }
        public double Price { get; set; }
        public string CoverImg { get; set; }
        public BsonDateTime PremiereDate { get; set; }
    }
}
