﻿using System;
using MongoDB.Bson;
using System.Collections.Generic;
using MongoRepository;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public class Actor : Entity
    {
        public string Name { get; set; }
        public string PhotoImg { get; set; }
        public IList<Movie> Movies { get; set; }
        public IList<Serial> Serials { get; set; }

        public Actor()
        {
            Movies = new List<Movie>();
            Serials = new List<Serial>();
        }
    }
}