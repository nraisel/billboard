﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public interface IBaseObject
    {
        ObjectId Id { get; set; }
    }
}
