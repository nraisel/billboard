﻿using System;
using MongoDB.Bson;
using System.Collections.Generic;
using MongoRepository;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public class Genre : Entity
    {
        public string Name { get; set; }
    }
}