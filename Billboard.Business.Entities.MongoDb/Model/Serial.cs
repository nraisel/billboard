﻿using System.Collections.Generic;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public class Serial : VideoHD
    {
        public IList<Actor> Actors { get; set; }
        public virtual IList<Season> Seasons { get; set; }
        public Serial()
        {
            Seasons = new List<Season>();
            Actors = new List<Actor>();
        }
    }
}