﻿using System;
using MongoDB.Bson;
using System.Collections.Generic;
using MongoRepository;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public class Season : Entity
    {
        public int SeasonNumber { get; set; }
        public int NumberOfEpisodes { get; set; }
        public Serial Serial { get; set; }
        public IList<Episode> Episodes { get; set; }

        public Season()
        {
            Episodes = new List<Episode>();
        }
    }
}