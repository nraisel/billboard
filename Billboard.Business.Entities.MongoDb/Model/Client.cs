﻿using System;
using MongoDB.Bson;
using System.Collections.Generic;
using MongoRepository;

namespace Billboard.Business.Entities.MongoDb.Model
{
    public class Client : Entity
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string estado { get; set; }
    }
}